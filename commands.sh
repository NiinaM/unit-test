mkdir -p ~/Asiakirjat/AMK/Yllapito-ja-testaus/L03/unit-test
code ~/Asiakirjat/AMK/Yllapito-ja-testaus/L03/unit-test

git init
npm init -y
npm install --save express
npm install --save-dev mocha chai

echo "node_modules" >> .gitignore

# git remote add origin https://gitlab.com/NiinaM/unit-test
# git push -u origin main

touch README.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js
