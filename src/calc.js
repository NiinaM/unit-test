/**
 * add two numbers
 * @param {number} a
 * @param {number} b
 * @returns {number}
 */
const add = (a, b) => a + b;

/**
 * subtract number from minuend
 * @param {number} minuend
 * @param {number} subtrahend
 * @returns {number} difference
 */
const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};

/**
 * 
 * @param {number} multiplier 
 * @param {number} multiplicand 
 * @returns {number} product
 */
const multiply = (multiplier, multiplicand) => {
    return multiplier * multiplicand;
};

/**
 * 
 * @param {number} dividend 
 * @param {number} divisor 
 * @returns {number}
 * @throws {Error} 0 division
 */
const divide = (dividend, divisor) => {
    if (divisor == 0) throw new Error("0 division not allowed");
    try {
        const fraction = dividend / divisor;
        return fraction;
    } catch (err) {
        throw new Error("0 division not allowed");
    }
};

export default { add, subtract, multiply, divide}